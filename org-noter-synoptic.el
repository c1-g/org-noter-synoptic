;;; org-noter-synoptic.el --- Read multiple documents with org-noter -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Dependencies and Setup
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(require 'org-noter)
(require 'text-clone)

(defcustom org-noter-keyword-companion-note "NOTER_COMPANION"
  "Name of the keyword that specifies additional notes or documents to read synoptically."
  :group 'org-noter
  :type 'string)

(defcustom org-noter-synoptic-use-cite-key (featurep 'org-roam-bibtex)
  "When non-nil, `org-noter-synoptic-extract-companion-keywords' will try to find the note files
associated with citekey.

Default is nil i.e. ignore the citekey.
Used by `org-noter-synoptic-extract-companion-keywords' to extract file names from
`org-noter-keyword-companion-note' keyword in current buffer."
  :group 'org-noter
  :type 'boolean)

(defvar org-noter-synoptic-companions nil
  "List of companions for the current buffer.")

(defvar org-noter-synoptic-transclusions nil
  "List of text-clone overlays of companions for the current buffer.")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Utilities functions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun random-elt (list)
  "The Emacs Lisp alternative for `seq-random-elt' which uses `cl'.

Return a random element from LIST. Will return nil if LIST is empty."
  (nth (random (length list)) list))

(defun cdrassq (value alist)
  "The true reverse of `assq', like `rassq' but works only with an ALIST
with lists as elements"
  (assq value (mapcar #'reverse alist)))

(defun color-palette (color &optional scheme distance variant percent step interactive)
  "Return a list of colors based on COLOR depending on SCHEME and VARIANT.
The additional DISTANCE may be supplied to change settings in some
SCHEME that uses it.

The additional PERCENT will be use to alter the effect of VARIANT.

SCHEME has to be one of these symbols 
  `mono' (monochromatic) - The monochromatic scheme is based on
  selecting a single hue on the color wheel, then adding more colors
  by varying the source color's saturation and brightness. 4 colors
  will be produced. This is the default.

  `contrast' - Constrast supplements the selected hue with its
complement (the color opposite it on the color wheel) as another
source color. 8 colors will be produced.

  `adjacent' - Produces colors that are analogous, or next to each
other on the color wheel.  User can increase the distance to push the
colors away from each other by using DISTANCE.  Some says values
between 15 and 30 (15-30 degrees on the wheel) are optimal. 12 colors
will be produced.

  `triade' - Triade takes the selected hue and adds two more sources
that are both a certain distance from the initial hue. User can
specify this distance by supplying DISTANCE. 12 colors will be
produced.

  `tetrade' (double complimentary) - Uses four colors arranged into
  two complimentary color pairings, it's called tetradic because it
  forms a tetrad in the color wheel. 16 colors will be produced.

VARIANT will alter the produced colors. They work exactly like filters
would in any image editing program. Possible values are 

  `default' - No change to the colors

  `pastel' - Produces pastel colors, which have in HSV high value and
low-intermediate saturation.

  `soft' - Produces darker pastel colors.

  `light' - Very light, almost washed-out colors.

  `hard' - Deeper, very saturated colors.

  `pale' - Colors with more gray; less saturated.

DISTANCE used in `adjacent'/`triade'/`tetrade' scheme (Because `mono'
 only has one source color and with `contrast' the two source colors
 are always 180 degrees away from each other.) sets the distance of
 the additional source colors from the initial hue. The value must be
 between 0 to 360."
  (interactive
   (list (read-color "Generate color palette based on: ")
         ;; TODO: Add more prompts here.
         nil
         nil
         nil
         nil
         nil
         t))
  (pcase-let* ((base-rgb (color-name-to-rgb color))
               (base-hsl (apply #'color-rgb-to-hsl base-rgb))
               (`(,bh ,bs ,bl) (append (list (mod (* (car base-hsl) 360) 360))
                                       (cdr base-hsl)))
               (`(,oh ,os ,ol) (list (mod (+ bh 180) 360) bs bl))
               (scheme (or scheme 'mono))
               (variant (or variant 'default))
               (distance (or distance 30))
               (percent (or percent 30))
               (step (or step 5))
               (steps (list (* step -2) (- step) step (* step 2)))
               (altered-palette nil)
               (palette)
               (base-colors))
    (setq colors-hsl
          (pcase scheme
            ('mono (list (list bh bs bl)))
            ('contrast (list (list bh bs bl) (list oh os ol)))
            ('adjacent (list (list (mod (- bh distance) 360) bs bl)
                             (list bh bs bl)
                             (list (mod (+ bh distance) 360) bs bl)))
            ('triade (list (list bh bs bl)  
                           (list (mod (- oh distance) 360) os ol)
                           (list (mod (+ oh distance) 360) os ol)))
            ('tetrade (list (list bh bs bl)
                            (list (mod (- bh distance) 360) bs bl)
                            (list oh os ol)
                            (list (mod (- oh distance) 360) os ol)))))

    (dolist (color-hsl colors-hsl)
      (dotimes (i 4)
        (let ((normalized-arg (append (list (/ (float (car color-hsl)) 360))
                                      (cdr color-hsl)
                                      (list (nth i steps)))))
          (push (apply #'color-lighten-hsl normalized-arg) palette)))
      (push
       (mapcar #'(lambda (hsl)
                   (apply #'color-rgb-to-hex
                          (apply #'color-hsl-to-rgb hsl)))
               (mapcar
                #'(lambda (hsl)
                    (pcase variant
                      ('default hsl)
                      ('pastel (apply #'color-desaturate-hsl (append hsl (list percent))))
                      ('soft (apply #'color-darken-hsl
                                    (append
                                     (apply #'color-desaturate-hsl
                                            (append hsl (list percent)))
                                     (list percent))))
                      ('hard (apply #'color-saturate-hsl (append hsl (list percent))))
                      ('pale (apply #'color-darken-hsl (append hsl (list percent))))
                      ('light (apply #'color-lighten-hsl (append hsl (list percent))))))
                palette))
       altered-palette)
      (setq palette nil))
    (when interactive (message
                       (concat
                        "("
                        (mapconcat #'(lambda (hex)
                                       (propertize "          "
                                                   'face `(:background ,hex)))
                                   (flatten-tree altered-palette) "")
                        ")")))  
    (flatten-tree altered-palette)))

(defun org-noter-synoptic-is-buf-or-file-org-p (buf-or-file)
  (when (bufferp buf-or-file)
    (setq buf-or-file (buffer-file-name buf-or-file)))
  (string= (file-name-extension buf-or-file) "org"))

(defun org-noter-synoptic-is-companion-p (file-name-or-citekey &optional force-find-citekey file-buf)
  "Check if FILE-NAME-OR-CITEKEY is in the value of  `org-noter-keyword-companion-note' keyword in FILE-BUF (or the current buffer if not specified).


This function matches all the buffer string against an org-keyword syntax and when user specified
a citekey it will also try to match the note path associated with the citekey depending on `org-noter-synoptic-use-cite-key' or the FORCE-FIND-CITEKEY argument.

If the match is more than zero return t, nil otherwise."
  (let ((find-citekey (or force-find-citekey org-noter-synoptic-use-cite-key))
        (key))
    (with-current-buffer (or file-buf (current-buffer))
      (not (zerop (how-many
                   ;; All these just to build a regexp, comes in 3 parts
                   (concat
                    ;; This part matches exactly this, with the default settings
                    ;; "#+NOTER_COMPANION:..........."
                    ;; or
                    ;; "#+noter_companion:..........."
                    (rx
                     line-start (* space)
                     "#+" (or (eval org-noter-keyword-companion-note)
                              (eval (downcase org-noter-keyword-companion-note)))
                     ":" (? (+? not-newline)))
                    ;; The filename or citekey
                    ;; "path/to/my/book.pdf"
                    ;; or
                    ;; "cite:exampleauthor2021"
                    file-name-or-citekey
                    ;; And an optional file path for citekey
                    ;; "\\|path/to/my/note/of/exampleauthor2021.org"
                    ;; ^^^^ This is an "or" syntax for regexp
                    (when find-citekey
                      (and
                       (string-match org-ref-cite-re file-name-or-citekey)
                       (setq key (match-string 2 file-name-or-citekey))
                       (concat "\\|" (orb-find-note-file key)))))
                   ;; (point-min) just so that HOW-MANY start matching
                   ;; at the top of the buffer
                   (point-min)))))))

(defun org-noter-synoptic-extract-companion-keywords (&optional buf)
  "Return a list of files from org-noter-property-companion-note in BUF (or the current buffer)."
  ;; Get the list of all values
  ;; Values are split in two ways
  ;; 1. with spaces and double quotes:
  ;;     #+PROP: a b c d "quoted string"
  ;;        => '(("PROP" "a" "b" "c" "d" "quoted string"))
  ;; 2. and/or with multiple lines:
  ;;     #+PROP: a b
  ;;     #+PROP: c d
  ;;        => '(("PROP" "a" "b" "c" "d"))
  (with-current-buffer (or buf (current-buffer))
    (let* ((raw-values (org-collect-keywords `(,org-noter-keyword-companion-note)))
           ;; ^ An example of the usual evaluation, with defaults setting,
           ;; #+NOTER_COMPANION: /path/to/my/book.pdf /path/to/another/book.epub
           ;; #+NOTER_COMPANION: cite:exampleauthor2021
           ;; => '(("NOTER_COMPANION" "path/to/my/book.pdf /path/to/another/book.epub" "cite:exampleauthor2021"))
           (values-list (delete-dups (cdar raw-values)))
           ;; ^ Results only the list of only values
           ;; => '("path/to/my/book.pdf /path/to/another/book.epub" "cite:exampleauthor2021")
           ;; NOTE: ^^^^^^^^^^^^^^^^^^^^ Notice that the file path are a single long string?
           (files '()))
      (setq values-list (mapcan #'split-string-and-unquote values-list))
      ;; Split the long string,
      ;; => '("path/to/my/book.pdf" "/path/to/another/book.epub" "cite:exampleauthor2021")
      (dolist (elt values-list)
        ;; And for each string in the list, do this,
        ;; Does the string have file syntax e.g. "/dir/dir/file.ext" etc.?
        (when (string-match-p ".*/.*" elt)
          ;; Yes, it's a file, check this conditions.
          (cond
           ;; Does it not exist?
           ((not (file-regular-p elt))
            (user-error "org-noter-synoptic-extract-companion-keywords on %s: Companion file %s doesn't exist!" buffer-file-name elt)
            ;; Is it unreadable?
            (not (file-readable-p elt))
            (user-error "org-noter-synoptic-extract-companion-keywords on %s: Can't read companion file %s!" buffer-file-name elt))
           ;; This file exists and is readable, add it to (FILES)
           (t (push elt files))))
        
        ;; The string doesn't have a file syntax,
        ;; when `org-noter-synoptic-use-cite-key' is non-nil, check if it's an org-ref's citekey.
        (when (and org-noter-synoptic-use-cite-key (featurep 'org-roam-bibtex))
          (orb-make-notes-cache)
          ;; Check by matching the string against org-ref's citekey regexp
          ;; It matches, it's a citekey, find note associated with it
          (if-let ((key (if (string-match org-ref-cite-re elt)
                            (match-string 2 elt)
                          elt))
                   (note-path (orb-find-note-file key)))
              ;; Note found, add it to the list (FILES)
              (push note-path files)
            ;; Note not found, ask user to create a note?
            (when (yes-or-no-p (format "Note for %s not found, create a new one with org-roam-bibtex? " elt))
              ;; Yeah, user wants to create it.
              (push (orb-edit-notes key) files)
              (warn "Can't find note for %s" elt)))))
      ;; Return (FILES)
      (flatten-list files))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;; Taking care of the global property
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Stolen code
;; Get list of global properties
;; From:
;; https://emacs.stackexchange.com/questions/21713/how-to-get-property-values-from-org-file-headers

(defun org-noter-synoptic--global-props (&optional name buffer)
  "Get the plists of global org properties by NAME in BUFFER.

Default NAME is \"PROPERTY\", default BUFFER the current buffer."
  (unless name (setq name "PROPERTY"))
  (with-current-buffer (or buffer (current-buffer))
    (org-element-map (org-element-parse-buffer) 'keyword
      (lambda (el) (when (string-match name (org-element-property :key el)) el))
      nil t)))

(defun org-noter-synoptic--get-global-prop (name &optional buffer)
  "Get global property by NAME in BUFFER (or current buffer in not specified)"
  (plist-get (car (cdr (org-noter-synoptic--global-props name buffer))) :value))

(defun org-noter-synoptic--add-global-prop (name value &optional buffer)
  "Concatenate global property by NAME with VALUE in BUFFER (or current buffer in not specified)"
  (with-current-buffer (or buffer (current-buffer))
    (let* ((prop (car (cdr (org-noter-synoptic--global-props name buffer))))
           (old-val (plist-get prop :value))
           (beg (plist-get prop :begin))
           (end (plist-get prop :end)))
      ;; Check if this buffer has the property in the first place
      (save-excursion
        (if (null old-val)
            ;; It doesn't, insert it automatically at the top of the buffer.
            (org-with-point-at (goto-line 2)
              (insert "#+" org-noter-keyword-companion-note ": " value "\n"))
          (org-with-point-at beg
            (let ((replace-lax-whitespace t)
                  (case-fold-search nil)))
            (re-search-forward (concat "#\\+" org-noter-keyword-companion-note ":\\(.*\\)$"))
            (replace-match (concat old-val " " value) nil nil nil 1))))
      (save-buffer))))

(defun org-noter-synoptic--find-companion ()
  "Open all companions in the keyword `org-noter-keyword-companion-note' of the current buffer.

And transclude their content to the current buffer also."
  (let ((this-buf (current-buffer))
        (this-file-path-or-key
         (if org-noter-synoptic-use-cite-key
             (org-noter-synoptic--get-global-prop "ROAM_KEY")
           (buffer-file-name)))
        (companion-buf-list
         (mapcar #'find-file-noselect
                 (org-noter-synoptic-extract-companion-keywords)))
        ;; Generate a color pallete based on a random color, no grays.
        (overlay-pallete (org-synoptic-transclusion-color)))
    
    (make-local-variable 'org-noter-synoptic-companions)
    (make-local-variable 'after-save-hook)
    (make-local-variable 'delete-frame-functions)
    (make-local-variable 'kill-buffer-hook)
    (dolist (note-buf (flatten-list (list (buffer-base-buffer this-buf) this-buf)))
      (with-current-buffer note-buf
        (setq org-noter-synoptic-companions companion-buf-list)
        (add-hook 'after-save-hook 'org-noter-synoptic--save-others nil t)
        (add-hook 'delete-frame-functions 'org-noter-synoptic--finish nil t)
        (add-hook 'kill-buffer-hook 'org-noter-synoptic--finish nil t)
        (add-hook 'kill-buffer-hook 'org-noter-synoptic--save-others)))
    (setq org-noter-synoptic-transclusions nil)
    ;; Add this file as a companion to other document if needed.
    (dolist (file-buf companion-buf-list)
      ;; Okay, now consider each buffer in COMPANION-BUF-LIST
      (with-current-buffer file-buf
        ;; Only do `org-noter-synoptic--add-global-prop' when all of the answers are yes.
        (when (and (file-writable-p (buffer-file-name file-buf))
                   ;; Is this buffer writable?
                   (org-noter-synoptic-is-buf-or-file-org-p file-buf)
                   ;; Is it an org file?
                   (not (org-noter-synoptic-is-companion-p
                         this-file-path-or-key nil file-buf))
                   ;; Is the companion list of this org file out-of-sync?
                   )
          ;; Add THIS-FILE-PATH-OR-KEY key as its companion.
          (org-noter-synoptic--add-global-prop org-noter-keyword-companion-note
                                               this-file-path-or-key
                                               file-buf))
        ;; Transclude the text from COMPANION-BUF-LIST to this buffer
        (org-synpotical-tranclude-text-from file-buf this-buf
                                            ;; Pick a random color from the palette
                                            (random-elt overlay-pallete))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;; Transcluding companions' content
(defun org-synpotical-tranclude-text-from (from-buf &optional to-buf overlay-color)
  "Tranclude the content of FROM-BUF to TO-BUF (current buffer if not
  specified) using the `text-clone' overlay.

The text are kept identical across the two buffers. May add a background color to
the overlay if OVERLAY-COLOR is specified."
  (let ((target-buf (or to-buf (current-buffer)))
        src-beg src-end src-content src-len src-ov clone-ov overlays)
    (with-current-buffer from-buf
      ;; Set SRC-BEG and SRC-END to the whole buffer initially.
      (setq src-beg (point-min))
      (setq src-end (point-max))
      (org-with-wide-buffer
       (when (org-find-property org-noter-property-doc-file)
         ;; Go to the first heading that has NOTER_DOCUMENT property
         ;; and also set SRC-BEG too.
         (goto-char (setq src-beg (org-find-property org-noter-property-doc-file)))
         (setq src-end (plist-get (cadr (org-element-at-point)) :end))))
      (setq src-len (- src-end src-beg))
      (setq src-content (buffer-substring src-beg src-end))
      (setq src-ov (text-clone-make-overlay src-beg src-end)))
    (with-current-buffer target-buf
      (org-with-wide-buffer
       (goto-char (point-max))
       (insert "\n#+BEGIN_COMMENT " (file-name-base (buffer-file-name from-buf)) "\n")
       (let ((transclusion-beg (point)))
         (insert "#+END_COMMENT " (file-name-base (buffer-file-name from-buf)) "\n")
         (goto-char transclusion-beg)
         (insert src-content)
         (setq clone-ov (text-clone-make-overlay
                         transclusion-beg (+ transclusion-beg src-len)))
         (push (text-clone-set-overlays src-ov clone-ov) org-noter-synoptic-transclusions))))
    (when overlay-color
      (dolist (overlay (car org-noter-synoptic-transclusions))
        (overlay-put overlay 'face
                     (list :background overlay-color :extend t))))
    from-buf))

(defun org-synoptic-transclusion-color (&optional color)
  "Return a list of colors based on COLOR."
  (let* ((color (or color (random-elt (flatten-list
                                       (mapcar
                                        #'(lambda (color)
                                            (when (not (color-gray-p color))
                                              color))
                                        (defined-colors)))))))
    (apply #'color-palette (append (list color)
                                   (pcase (frame-parameter (selected-frame) 'background-mode)
                                     ('dark '(adjacent nil soft))
                                     ('light '(adjacent nil pastel)))))))

;;;; User commands
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;###autoload
(defun org-noter-synoptic-add-companion (file-name-or-citekey)
  "Add FILE-NAME-OR-CITEKEY to the current org buffer."
  (interactive"P")
  (if (string= (file-name-extension buffer-file-name) "org")
      (let ((file-name-or-citekey
             (if (and org-noter-synoptic-use-cite-key
                      (string=  "Citekey" (completing-read "Add companion note: "
                                                           (list "File-name" "Citekey"))))
                 (progn (bibtex-completion-init)
                        (concat "cite:"
                                (assoc-default "=key="
                                               (alist-get
                                                (completing-read
                                                 "Add companion citekey: "
                                                 (bibtex-completion-candidates) nil t)
                                                (bibtex-completion-candidates) "" nil #'string=))))
               (read-file-name "Add companion file: "))))
        (org-noter-synoptic--add-global-prop org-noter-keyword-companion-note file-name-or-citekey))
    (user-error "Not a buffer visiting an org file.")))

(defun org-noter-synoptic--save-others ()
  "Save every buffer in this buffer's `org-noter-synoptic-companions'"
  (interactive)
  (dolist (companion org-noter-synoptic-companions)
    (with-current-buffer companion
      (save-buffer))))

(defun org-noter-synoptic--finish ()
  "Remove the overlay, the trancluded text. Leave only the original text."
  (interactive)
  (dolist (overlay-in-this-buf (delete-dups (flatten-tree (overlay-lists))))
    (let ((ovs-pair (or (assq overlay-in-this-buf org-noter-synoptic-transclusions)
                        (cdrassq overlay-in-this-buf org-noter-synoptic-transclusions))))
      (dolist (ov ovs-pair)
        (when (and ov (overlay-buffer ov))
          (with-current-buffer (overlay-buffer ov)
            (let ((start (overlay-start ov))
                  (end (overlay-end ov))
                  (name-base (file-name-base
                              (buffer-file-name
                               (overlay-buffer
                                (cadr ovs-pair))))))
              (delete-overlay ov)
              (unless (memq (current-buffer) org-noter-synoptic-companions)
                (org-with-wide-buffer
                 (goto-char start)
                 (beginning-of-line)
                 (previous-line 2)
                 (let ((case-fold-search t)
                       (search-upper-case nil))
                   (delete-matching-lines (concat "#\\+begin_comment "
                                                  name-base
                                                  "[^z-a]*"
                                                  "#\\+end_comment "
                                                  name-base)
                                          (point) (point-max)))
                 (delete-trailing-whitespace (point) (point-max))
                 (save-buffer))))))))))

(provide 'org-noter-synoptic)
;;; org-noter-synoptic.el ends here
